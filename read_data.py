import yaml
import uproot
import ROOT

with open("config.yml") as file:
    config = yaml.safe_load(file)

def read_from_path(filepath, tree = "DecayTree"):
    with uproot.open(filepath) as file:
        arrays = file[tree].arrays(library="np")
    return arrays

def get_data(file, keys="",  tree="DecayTree"):
    """
    Get data from a root file
    """
    ROOT.EnableImplicitMT()
    df = ROOT.RDataFrame(tree, file)
    return df.AsNumpy(keys)

def get_all_keys(file, tree="DecayTree"):
    """
    Get all keys from a root file
    """
    df = ROOT.RDataFrame(tree, file)
    return [str(k) for k in df.GetColumnNames()]

def read_from_path(filepath, tree = "DecayTree"):
    return get_data(filepath, get_all_keys(filepath,tree=tree) , tree=tree)

def read_rapidsim(name):
    filepath = config["data"]["RapidSym"][name]
    return read_from_path(filepath=filepath)

def read_LHCbMC(name):
    filepath = config["data"]["LHCbMC"][name]
    return read_from_path(filepath=filepath)

# print(read_rapidsim("Xib02Xi0n0pminusKplus"))

