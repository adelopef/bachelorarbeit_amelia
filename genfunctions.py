# for P0 -> {P1 -> S, P1A, P1B} 

import matplotlib.pyplot as plt
from read_data import read_rapidsim
import numpy as np
import array as arr

def Q(M_P0, M_P1, M_P2):
    return 0.5 * ( M_P0**2 - M_P1**2 - M_P2**2)


def unitvec(p):
    p = p/np.linalg.norm(p, axis=0)
    return p

def scalar_product(a,b):
    # expects arrays of shape (3, n)
    return np.sum(a * b, axis=0)

def dir(vtx_Xibm, vtx_Xib0):
    return unitvec(np.subtract(np.array(vtx_Xibm),np.array(vtx_Xib0)))

def D(M_P1, M_P2, E_P2, V, M_P0):
    return (16 * Q(M_P0, M_P1, M_P2)**2 * E_P2**2 * V**2 * (Q(M_P0, M_P1, M_P2)**2 - M_P1**2 * (E_P2**2 - V**2)))  


def qboth(M_P1, M_P2, E_P2, V, M_P0, solution ): 
    q1 = (Q(M_P0, M_P1, M_P2)**2 * (E_P2**2+V**2) - M_P1**2 * E_P2**2 * (E_P2**2-V**2) + np.sqrt(D(M_P1, M_P2, E_P2, V, M_P0)))/( 2 * (E_P2**2-V**2)**2)
    q2 = (Q(M_P0, M_P1, M_P2)**2 * (E_P2**2+V**2) - M_P1**2 * E_P2**2 * (E_P2**2-V**2) - np.sqrt(D(M_P1, M_P2, E_P2, V, M_P0)))/( 2 * (E_P2**2-V**2)**2)
    if solution == 1:
        return q1
    if solution == 2:
        return q2
    if solution == 3:
        return np.where(q1 >=0, q1,q2)

    raise ValueError(f"Solution must be either 1 , 2 or 3 not {solution}!")




def E_s(M_P1, M_P2,E_P2,V, M_P0, E_P1B, E_P1A,solution ): 
    return (np.sqrt(M_P1**2 + qboth(M_P1, M_P2,E_P2,V,M_P0,solution ) ) - E_P1B - E_P1A)

def p_s(M_P1, M_P2, E_P2, V, M_P0 ,p_Xib0, p_P1B, p_P1A, solution ):
    v = unitvec(p_Xib0)
    return np.sqrt(qboth(M_P1, M_P2, E_P2, V, M_P0,solution))*v - p_P1B - p_P1A


def P_abs(M_P1, M_P2,E_P2,V, M_P0, E_P1B, E_P1A, p_Xib0, p_P1B, p_P1A,solution ):
    x = np.sqrt(E_s(M_P1, M_P2,E_P2,V, M_P0, E_P1B, E_P1A, solution )**2 - np.sum(p_s(M_P1, M_P2, E_P2, V, M_P0, p_Xib0, p_P1B, p_P1A, solution )**2, axis = 0 ))
    return x

def infvals(M_P1, M_P2,E_P2,V, M_P0, E_P1B, E_P1A, p_Xib0, p_P1B, p_P1A,solution):
    x = P_abs(M_P1, M_P2,E_P2,V, M_P0, E_P1B, E_P1A, p_Xib0, p_P1B, p_P1A,solution )
    y = np.where(np.isfinite(x), x , -3 * np.ones_like(x))
    return np.count_nonzero(y == -3)


