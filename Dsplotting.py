import matplotlib.pyplot as plt
from read_data import read_rapidsim
from functions import P_abs, unitvec, scalar_product, infvals, dir
import numpy as np


#data from simulation for S
data = read_rapidsim("ZerfallDs")

p_pip = [data["pip_0_PX"],data["pip_0_PY"],data["pip_0_PZ"]]
E_pip = data["pip_0_E"]
p_Kp = [data["Kp_0_PX"],data["Kp_0_PY"],data["Kp_0_PZ"]]
E_Kp = data["Kp_0_E"]
p_Km = [data["Km_0_PX"],data["Km_0_PY"],data["Km_0_PZ"]]
E_Km = data["Km_0_E"]
p_Kaon= np.add(p_Kp, np.add(p_Km ,p_pip ))#[UGXi0["Dsp_0_PX"],UGXi0["Dsp_0_PY"],UGXi0["Dsp_0_PZ"]]
E_Kaon = E_Kp + E_Km + E_pip#UGXi0["Dsp_0_E"]

p_proton = [data["pm_0_PX"],data["pm_0_PY"],data["pm_0_PZ"]]
E_proton = data["pm_0_E"]
ppi =  [data["pim_0_PX"],data["pim_0_PY"],data["pim_0_PZ"]]
E_pi = data["pim_0_E"]
E = E_pi
vtx_Xib0= [data["Xib0_0_vtxX"], data["Xib0_0_vtxY"], data["Xib0_0_vtxZ"]]
vtx_Xibm = [data["Xib'm_0_vtxX"],data["Xib'm_0_vtxY"],data["Xib'm_0_vtxZ"]]
dir_Xib0 = dir(vtx_Xib0, vtx_Xibm)
#p_Xi0b is not known, but its direction is. The simulated data for p_Xi0b is used to get the flight direction as unit vector
p_Xib0 = [data["Xib0_0_PX"], data["Xib0_0_PY"], data["Xib0_0_PZ"]]
#previously calculated values
Q = 0.829439 #GeV²
M = 5.7919#GeV Mass of Xib'-
V = scalar_product(ppi, unitvec(dir_Xib0))
plt.figure(figsize=(10,5), dpi=80)
n, bins, _ = plt.hist(P_abs(Q,E,V, M, E_proton, E_Kaon, dir_Xib0, p_proton, p_Kaon,solution =1)
                      ,histtype="step",bins=100, color = 'b', label = 'S1875, '+str(infvals(Q,E,V, M, E_proton, E_Kaon, dir_Xib0, p_proton, p_Kaon,solution = 1))+' inf values, p_Xib0 method')



#######################################################################################################

#Data for background decays: Lambda Lambda 
UGlambda =read_rapidsim("UGLambdaDS")

p_pip = [UGlambda["pip_0_PX"],UGlambda["pip_0_PY"],UGlambda["pip_0_PZ"]]
E_pip = UGlambda["pip_0_E"]
p_Kp = [UGlambda["Kp_0_PX"],UGlambda["Kp_0_PY"],UGlambda["Kp_0_PZ"]]
E_Kp = UGlambda["Kp_0_E"]
p_Km = [UGlambda["Km_0_PX"],UGlambda["Km_0_PY"],UGlambda["Km_0_PZ"]]
E_Km = UGlambda["Km_0_E"]

p_Kaon= np.add(p_Kp, np.add(p_Km ,p_pip ))#[UGXi0["Dsp_0_PX"],UGXi0["Dsp_0_PY"],UGXi0["Dsp_0_PZ"]]
E_Kaon = E_Kp + E_Km + E_pip#UGXi0["Dsp_0_E"]
p_proton = [UGlambda["pm_0_PX"],UGlambda["pm_0_PY"],UGlambda["pm_0_PZ"]]
E_proton = UGlambda["pm_0_E"]
ppi =  [UGlambda["pim_0_PX"],UGlambda["pim_0_PY"],UGlambda["pim_0_PZ"]]
E_pi = UGlambda["pim_0_E"]
E = E_pi
vtx_Xib0= [UGlambda["Xib0_0_vtxX"], UGlambda["Xib0_0_vtxY"], UGlambda["Xib0_0_vtxZ"]]
vtx_Xibm = [UGlambda["Xib'm_0_vtxX"],UGlambda["Xib'm_0_vtxY"],UGlambda["Xib'm_0_vtxZ"]]
dir_Xib0 = dir(vtx_Xib0, vtx_Xibm)
#p_Xi0b is not known, but its direction is. The simulated UGlambda for p_Xi0b is used to get the flight direction as unit vector
p_Xib0 = [UGlambda["Xib0_0_PX"], UGlambda["Xib0_0_PY"], UGlambda["Xib0_0_PZ"]]

V = scalar_product(ppi, unitvec(dir_Xib0))

n, bins, _ = plt.hist(P_abs(Q,E,V, M, E_proton, E_Kaon, dir_Xib0, p_proton, p_Kaon,solution =1)
                      ,histtype="step",bins=100, color = 'r', label = '$\Lambda \Lambda$, ' +str(infvals(Q,E,V, M, E_proton, E_Kaon, dir_Xib0, p_proton, p_Kaon,solution = 1))+' inf values, p_Xib0 method')


###################################################################

#Data for background decays:  Xi0 n
UGXi0=read_rapidsim("UGXi0DS")
p_pip = [UGXi0["pip_0_PX"],UGXi0["pip_0_PY"],UGXi0["pip_0_PZ"]]
E_pip = UGXi0["pip_0_E"]
p_Kp = [UGXi0["Kp_0_PX"],UGXi0["Kp_0_PY"],UGXi0["Kp_0_PZ"]]
E_Kp = UGXi0["Kp_0_E"]
p_Km = [UGXi0["Km_0_PX"],UGXi0["Km_0_PY"],UGXi0["Km_0_PZ"]]
E_Km = UGXi0["Km_0_E"]


p_Kaon= np.add(p_Kp, np.add(p_Km ,p_pip ))#[UGXi0["Dsp_0_PX"],UGXi0["Dsp_0_PY"],UGXi0["Dsp_0_PZ"]]
E_Kaon = E_Kp + E_Km + E_pip#UGXi0["Dsp_0_E"]
p_proton = [UGXi0["pm_0_PX"],UGXi0["pm_0_PY"],UGXi0["pm_0_PZ"]]
E_proton = UGXi0["pm_0_E"]
ppi =  [UGXi0["pim_0_PX"],UGXi0["pim_0_PY"],UGXi0["pim_0_PZ"]]
E_pi = UGXi0["pim_0_E"]
E = E_pi
vtx_Xib0= [UGXi0["Xib0_0_vtxX"], UGXi0["Xib0_0_vtxY"], UGXi0["Xib0_0_vtxZ"]]
vtx_Xibm = [UGXi0["Xib'm_0_vtxX"],UGXi0["Xib'm_0_vtxY"],UGXi0["Xib'm_0_vtxZ"]]
dir_Xib0 = dir(vtx_Xib0, vtx_Xibm)
#p_Xi0b is not known, but its direction is. The simulated UGXi0 for p_Xi0b is used to get the flight direction as unit vector
p_Xib0 = [UGXi0["Xib0_0_PX"], UGXi0["Xib0_0_PY"], UGXi0["Xib0_0_PZ"]]

#previously calculated values

V = scalar_product(ppi, unitvec(dir_Xib0))

n, bins, _ = plt.hist(P_abs(Q,E,V, M, E_proton, E_Kaon, dir_Xib0, p_proton, p_Kaon,solution =1)
                      ,histtype="step",bins=100, color = 'g', label = '$n$ $\Xi_0$, ' +str(infvals(Q,E,V, M, E_proton, E_Kaon, dir_Xib0, p_proton, p_Kaon,solution = 1))+' inf values, p_Xib0 method')


plt.title('Sexaquarkmass $M_S$, 100 000 simulated decays, $\Xi_b^{\'}$ Tag, $D_s⁺$ mode With Background', fontsize=14, family ='monospace')
plt.ylabel('Events / Bin ', fontsize=14, family ='monospace')
plt.xlabel('$M_S$ [GeV]', fontsize=14, family ='monospace')
plt.legend()
plt.grid()
plt.show()



#plt.grid()

#centers = (bins[1:] + bins[:-1])/2
#plt.errorbar(centers, n, fmt=".")
#plt.show()





#data from simulation for S
data = read_rapidsim("ZerfallDs")
p_pip = [data["pip_0_PX_TRUE"],data["pip_0_PY_TRUE"],data["pip_0_PZ_TRUE"]]
E_pip = data["pip_0_E_TRUE"]
p_Kp = [data["Kp_0_PX_TRUE"],data["Kp_0_PY_TRUE"],data["Kp_0_PZ_TRUE"]]
E_Kp = data["Kp_0_E_TRUE"]
p_Km = [data["Km_0_PX_TRUE"],data["Km_0_PY_TRUE"],data["Km_0_PZ_TRUE"]]
E_Km = data["Km_0_E_TRUE"]
p_Kaon= np.add(p_Kp, np.add(p_Km ,p_pip ))#[UGXi0["Dsp_0_PX"],UGXi0["Dsp_0_PY"],UGXi0["Dsp_0_PZ"]]
E_Kaon = E_Kp + E_Km + E_pip#UGXi0["Dsp_0_E"]
p_proton = [data["pm_0_PX_TRUE"],data["pm_0_PY_TRUE"],data["pm_0_PZ_TRUE"]]
E_proton = data["pm_0_E_TRUE"]
ppi =  [data["pim_0_PX_TRUE"],data["pim_0_PY_TRUE"],data["pim_0_PZ_TRUE"]]
E_pi = data["pim_0_E_TRUE"]
E = E_pi
vtx_Xib0= [data["Xib0_0_vtxX_TRUE"], data["Xib0_0_vtxY_TRUE"], data["Xib0_0_vtxZ_TRUE"]]
vtx_Xibm = [data["Xib'm_0_vtxX_TRUE"],data["Xib'm_0_vtxY_TRUE"],data["Xib'm_0_vtxZ_TRUE"]]
dir_Xib0 = dir(vtx_Xib0, vtx_Xibm)
#p_Xi0b is not known, but its direction is. The simulated data for p_Xi0b is used to get the flight direction as unit vector
p_Xib0 = [data["Xib0_0_PX_TRUE"], data["Xib0_0_PY_TRUE"], data["Xib0_0_PZ_TRUE"]]
#previously calculated values
Q = 0.829439 #GeV²
M = 5.7919#GeV Mass of Xib'-
V = scalar_product(ppi, unitvec(dir_Xib0))
plt.figure(figsize=(10,5), dpi=80)
n, bins, _ = plt.hist(P_abs(Q,E,V, M, E_proton, E_Kaon, dir_Xib0, p_proton, p_Kaon,solution =1)
                      ,histtype="step",bins=100, color = 'b', label = 'S1875, '+str(infvals(Q,E,V, M, E_proton, E_Kaon, dir_Xib0, p_proton, p_Kaon,solution = 1))+' inf values, p_Xib0 method')


#######################################################################################################

#Data for background decays: Lambda Lambda 
UGlambda =read_rapidsim("UGLambdaDS")

p_pip = [UGlambda["pip_0_PX_TRUE"],UGlambda["pip_0_PY_TRUE"],UGlambda["pip_0_PZ_TRUE"]]
E_pip = UGlambda["pip_0_E_TRUE"]
p_Kp = [UGlambda["Kp_0_PX_TRUE"],UGlambda["Kp_0_PY_TRUE"],UGlambda["Kp_0_PZ_TRUE"]]
E_Kp = UGlambda["Kp_0_E_TRUE"]
p_Km = [UGlambda["Km_0_PX_TRUE"],UGlambda["Km_0_PY_TRUE"],UGlambda["Km_0_PZ_TRUE"]]
E_Km = UGlambda["Km_0_E_TRUE"]
p_Kaon= np.add(p_Kp, np.add(p_Km ,p_pip ))#[UGXi0["Dsp_0_PX"],UGXi0["Dsp_0_PY"],UGXi0["Dsp_0_PZ"]]
E_Kaon = E_Kp + E_Km + E_pip#UGXi0["Dsp_0_E"]

p_proton = [UGlambda["pm_0_PX_TRUE"],UGlambda["pm_0_PY_TRUE"],UGlambda["pm_0_PZ_TRUE"]]
E_proton = UGlambda["pm_0_E_TRUE"]
ppi =  [UGlambda["pim_0_PX_TRUE"],UGlambda["pim_0_PY_TRUE"],UGlambda["pim_0_PZ_TRUE"]]
E_pi = UGlambda["pim_0_E_TRUE"]
E = E_pi
vtx_Xib0= [UGlambda["Xib0_0_vtxX_TRUE"], UGlambda["Xib0_0_vtxY_TRUE"], UGlambda["Xib0_0_vtxZ_TRUE"]]
vtx_Xibm = [UGlambda["Xib'm_0_vtxX_TRUE"],UGlambda["Xib'm_0_vtxY_TRUE"],UGlambda["Xib'm_0_vtxZ_TRUE"]]
dir_Xib0 = dir(vtx_Xib0, vtx_Xibm)
#p_Xi0b is not known, but its direction is. The simulated UGlambda for p_Xi0b is used to get the flight direction as unit vector
p_Xib0 = [UGlambda["Xib0_0_PX_TRUE"], UGlambda["Xib0_0_PY_TRUE"], UGlambda["Xib0_0_PZ_TRUE"]]

V = scalar_product(ppi, unitvec(dir_Xib0))

n, bins, _ = plt.hist(P_abs(Q,E,V, M, E_proton, E_Kaon, dir_Xib0, p_proton, p_Kaon,solution =1)
                      ,histtype="step",bins=100, color = 'r', label = '$\Lambda \Lambda$, ' +str(infvals(Q,E,V, M, E_proton, E_Kaon, dir_Xib0, p_proton, p_Kaon,solution = 1))+' inf values, p_Xib0 method')

###################################################################

#Data for background decays:  Xi0 n
UGXi0=read_rapidsim("UGXi0DS")
p_pip = [UGXi0["pip_0_PX_TRUE"],UGXi0["pip_0_PY_TRUE"],UGXi0["pip_0_PZ_TRUE"]]
E_pip = UGXi0["pip_0_E_TRUE"]
p_Kp = [UGXi0["Kp_0_PX_TRUE"],UGXi0["Kp_0_PY_TRUE"],UGXi0["Kp_0_PZ_TRUE"]]
E_Kp = UGXi0["Kp_0_E_TRUE"]
p_Km = [UGXi0["Km_0_PX_TRUE"],UGXi0["Km_0_PY_TRUE"],UGXi0["Km_0_PZ_TRUE"]]
E_Km = UGXi0["Km_0_E_TRUE"]


p_Kaon= np.add(p_Kp, np.add(p_Km ,p_pip ))#[UGXi0["Dsp_0_PX_TRUE"],UGXi0["Dsp_0_PY_TRUE"],UGXi0["Dsp_0_PZ_TRUE"]]
E_Kaon = E_Kp + E_Km + E_pip#UGXi0["Dsp_0_E_TRUE"]
p_proton = [UGXi0["pm_0_PX_TRUE"],UGXi0["pm_0_PY_TRUE"],UGXi0["pm_0_PZ_TRUE"]]
E_proton = UGXi0["pm_0_E_TRUE"]
ppi =  [UGXi0["pim_0_PX_TRUE"],UGXi0["pim_0_PY_TRUE"],UGXi0["pim_0_PZ_TRUE"]]
E_pi = UGXi0["pim_0_E_TRUE"]
E = E_pi
vtx_Xib0= [UGXi0["Xib0_0_vtxX_TRUE"], UGXi0["Xib0_0_vtxY_TRUE"], UGXi0["Xib0_0_vtxZ_TRUE"]]
vtx_Xibm = [UGXi0["Xib'm_0_vtxX_TRUE"],UGXi0["Xib'm_0_vtxY_TRUE"],UGXi0["Xib'm_0_vtxZ_TRUE"]]
dir_Xib0 = dir(vtx_Xib0, vtx_Xibm)
#p_Xi0b is not known, but its direction is. The simulated UGXi0 for p_Xi0b is used to get the flight direction as unit vector
p_Xib0 = [UGXi0["Xib0_0_PX_TRUE"], UGXi0["Xib0_0_PY_TRUE"], UGXi0["Xib0_0_PZ_TRUE"]]

#previously calculated values

V = scalar_product(ppi, unitvec(dir_Xib0))

n, bins, _ = plt.hist(P_abs(Q,E,V, M, E_proton, E_Kaon, dir_Xib0, p_proton, p_Kaon,solution =1)
                      ,histtype="step",bins=100, color = 'g', label = '$n$ $\Xi_0$, ' +str(infvals(Q,E,V, M, E_proton, E_Kaon, dir_Xib0, p_proton, p_Kaon,solution = 1))+' inf values, p_Xib0 method')


plt.title('Sexaquarkmass $M_S$, 100 000 simulated decays, $\Xi_b^{\'}$ Tag, $D_s⁺$ mode With Background, TRUE values', fontsize=14, family ='monospace')
plt.ylabel('Events / Bin ', fontsize=14, family ='monospace')
plt.xlabel('$M_S$ [GeV]', fontsize=14, family ='monospace')
plt.legend()
plt.grid()
plt.show()



#plt.grid()

#centers = (bins[1:] + bins[:-1])/2
#plt.errorbar(centers, n, fmt=".")
#plt.show()




