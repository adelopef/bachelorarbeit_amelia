import matplotlib.pyplot as plt
from read_data import read_rapidsim
from functions import P_abs, unitvec, scalar_product, infvals, dir, qboth
import numpy as np

#data from simulation
data = read_rapidsim("Zerfall")
p_Kaon= [data["Kp_0_PX"],data["Kp_0_PY"],data["Kp_0_PZ"]]
E_Kaon = data["Kp_0_E"]
p_proton = [data["pm_0_PX"],data["pm_0_PY"],data["pm_0_PZ"]]
E_proton = data["pm_0_E"]
ppi =  [data["pim_0_PX"],data["pim_0_PY"],data["pim_0_PZ"]]
E_pi = data["pim_0_E"]
E = E_pi
vtx_Xib0= [data["Xib0_0_vtxX"], data["Xib0_0_vtxY"], data["Xib0_0_vtxZ"]]
vtx_Xibm = [data["Xib0_0_origX"],data["Xib0_0_origY"],data["Xib0_0_origZ"]]
dir_Xib0 = dir(vtx_Xib0, vtx_Xibm)
#p_Xi0b is not known, but its direction is. The simulated data for p_Xi0b is used to get the flight direction as unit vector
p_Xib0 = [data["Xib0_0_PX"], data["Xib0_0_PY"], data["Xib0_0_PZ"]]
print(np.shape(p_Xib0))

#previously calculated values
Q = 0.5 * (5.93502**2-5.7919**2- 0.13957018**2)  #GeV²
M = 5.7919#GeV Mass of Xib0
V = scalar_product(ppi, unitvec(dir_Xib0))
plt.figure(figsize=(10,5), dpi=80)

n, bins, _ = plt.hist( P_abs(Q,E,V, M, E_proton, E_Kaon, dir_Xib0, p_proton, p_Kaon,solution =1)
                      ,histtype="step",bins=100, color = 'r', label = '- solution, vtx method, '
                      + str(infvals(Q,E,V, M, E_proton, E_Kaon, dir_Xib0, p_proton, p_Kaon,solution = 1)) 
                      + ' infinite values')


#V = scalar_product(ppi, unitvec(p_Xib0))

#n, bins = np.histogram(ppi)
#n, bins, _ = plt.hist(ppi[0],histtype="step",bins=75)
# exit(0)

n, bins, _ = plt.hist(P_abs(Q,E,V, M, E_proton, E_Kaon, dir_Xib0, p_proton, p_Kaon,solution =1)
                      ,histtype="step",bins=100, color = 'b', label = '+ solution, vtx method .'
                      + str(infvals(Q,E,V, M, E_proton, E_Kaon, dir_Xib0, p_proton, p_Kaon,solution = 2)) 
                      + ' infinite values')



V = scalar_product(ppi, unitvec(p_Xib0))

# n, bins, _ = plt.hist(P_abs(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution =1)
#                       ,histtype="step",bins=100, color = 'cornflowerblue', label = '+ solution, with p_Xib0, '
#                       + str(infvals(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution = 1)) 
#                       + ' infinite values')

# n, bins, _ = plt.hist(P_abs(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution =2)
#                       ,histtype="step",bins=100, color = 'lightcoral', label = '- solution, with p_Xib0, '
#                       + str(infvals(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution = 2)) 
#                       + ' infinite values')



plt.title('Sexaquarkmass $M_S$, 100 000 simulated decays, $\Xi_b^{\'}$ Tag', fontsize=14, family ='monospace')
plt.ylabel('Events / Bin ', fontsize=14, family ='monospace')
plt.xlabel('$M_S$ [GeV]', fontsize=14, family ='monospace')
#plt.savefig('fitted_data.png')
plt.legend()
plt.grid()
plt.show()




#plt.grid()

#centers = (bins[1:] + bins[:-1])/2
#plt.errorbar(centers, n, fmt=".")
#plt.show()




