# (16144010) with M_S = 1.761 GeV is not possible for M_s < 1.759 GeV so S1761 has a Mass of 1.759 GeV

#data from simulation

import matplotlib.pyplot as plt
from read_data import read_rapidsim
from functions import P_abs, unitvec, scalar_product, infvals, dir
import numpy as np 


data = read_rapidsim("ZerfallJpsi")
p_pip= [data["pip_0_PX"],data["pip_0_PY"],data["pip_0_PZ"]]
E_pip = data["pip_0_E"]
p_Jspi = [data["Jpsi_0_PX"],data["Jpsi_0_PY"],data["Jpsi_0_PZ"]]
E_Jspi = data["Jpsi_0_E"]

p_Kaon =np.add(p_pip,p_Jspi)
E_Kaon = np.add(E_pip ,E_Jspi)

p_proton = [data["pm_0_PX"],data["pm_0_PY"],data["pm_0_PZ"]]
E_proton = data["pm_0_E"]
ppi =  [data["pim_0_PX"],data["pim_0_PY"],data["pim_0_PZ"]]
E_pi = data["pim_0_E"]
E = E_pi
vtx_Xib0= [data["Xib0_0_vtxX"], data["Xib0_0_vtxY"], data["Xib0_0_vtxZ"]]
vtx_Xibm = [data["Xib'm_0_vtxX"],data["Xib'm_0_vtxY"],data["Xib'm_0_vtxZ"]]
dir_Xib0 = dir(vtx_Xib0, vtx_Xibm)
#p_Xi0b is not known, but its direction is. The simulated data for p_Xi0b is used to get the flight direction as unit vector
p_Xib0 = [data["Xib0_0_PX"], data["Xib0_0_PY"], data["Xib0_0_PZ"]]

#previously calculated values
Q = 0.829439  #GeV²
M = 5.7919#GeV Mass of Xib'-
V = scalar_product(ppi, unitvec(dir_Xib0))
#V = scalar_product(ppi, unitvec(p_Xib0))

#n, bins = np.histogram(ppi)
#n, bins, _ = plt.hist(ppi[0],histtype="step",bins=75)
# exit(0)
plt.figure(figsize=(10,5), dpi=80)
n, bins, _ = plt.hist(P_abs(Q,E,V, M, E_proton, E_Kaon, dir_Xib0, p_proton, p_Kaon,solution =1)
                      ,histtype="step",bins=100, color = 'b', label = '+ solution, vtx method .'
                      + str(infvals(Q,E,V, M, E_proton, E_Kaon, dir_Xib0, p_proton, p_Kaon,solution = 1)) 
                      + ' infinite values')



#now with pxi0b

V = scalar_product(ppi, unitvec(p_Xib0))

n, bins, _ = plt.hist(P_abs(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution =1)
                      ,histtype="step",bins=100, color = 'cornflowerblue', label = '+ solution, with p_Xib0, '
                      + str(infvals(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution = 1)) 
                      + ' infinite values')


plt.title('Sexaquarkmass $M_S$, 100 000 simulated decays, $\Xi_b^{\'}$ Tag, $J/\psi$ final state', fontsize=14, family ='monospace')
plt.ylabel('Events / Bin ', fontsize=14, family ='monospace')
plt.xlabel('$M_S$ [GeV], $M_S$ = 1617 GeV for simulation', fontsize=14, family ='monospace')
#plt.savefig('fitted_data.png')
plt.legend()
plt.grid()
plt.show()
exit(0)
#####################################

E_proton = data["pm_0_E"]
ppi =  [data["pip_0_PX"],data["pip_0_PY"],data["pip_0_PZ"]]
E_pi = data["pip_0_E"]
E = E_pi
vtx_Xibm= [data["Xib05945_0_vtxX"], data["Xib05945_0_vtxY"], data["Xib05945_0_vtxZ"]]
vtx_Xib0 = [data["Xibm_0_vtxX"],data["Xibm_0_vtxY"],data["Xibm_0_vtxZ"]]
dir_Xib0 = dir(vtx_Xib0, vtx_Xibm)
#p_Xi0b is not known, but its direction is. The simulated data for p_Xi0b is used to get the flight direction as unit vector
p_Xib0 = [data["Xibm_0_PX"], data["Xibm_0_PY"], data["Xibm_0_PZ"]]

#previously calculated values
Q = 0.5*(5.9523**2-0.13957018**2-5.7945**2)#GeV²
M = 5.7945 #GeV Mass of Xib'-
V = scalar_product(ppi, dir_Xib0)
#V = scalar_product(ppi, unitvec(p_ M_S = 1617 GeV for simulationpe="step",bins=75)
# exit(0)
plt.figure(figsize=(10,5), dpi=80)
n, bins, _ = plt.hist(P_abs(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution =1)
                      ,histtype="step",bins=100, color = 'b', label = '+ solution, vtx method '
                      + str(infvals(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution = 1)) 
                      + ' infinite values')



#now with pxi0b

V = scalar_product(ppi, unitvec(p_Xib0))

n, bins, _ = plt.hist(P_abs(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution =1)
                      ,histtype="step",bins=100, color = 'cornflowerblue', label = '+ solution, with p_Xib, '
                      + str(infvals(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution = 1)) 
                      + ' infinite values')

##################################


data = read_rapidsim("Xi5945")
p_Jspi = [data["Jpsi_0_PX"],data["Jpsi_0_PY"],data["Jpsi_0_PZ"]]
E_Jspi = data["Jpsi_0_E"]

p_Kaon =p_Jspi
E_Kaon =E_Jspi

p_proton = [data["pm_0_PX"],data["pm_0_PY"],data["pm_0_PZ"]]
p_Kaon =p_Jspi
E_Kaon =E_Jspi

p_proton = [data["pm_0_PX"],data["pm_0_PY"],data["pm_0_PZ"]]
E_proton = data["pm_0_E"]
ppi =  [data["pip_0_PX"],data["pip_0_PY"],data["pip_0_PZ"]]
E_pi = data["pip_0_E"]
E = E_pi
vtx_Xibm= [data["Xib05945_0_vtxX"], data["Xib05945_0_vtxY"], data["Xib05945_0_vtxZ"]]
vtx_Xib0 = [data["Xibm_0_vtxX"],data["Xibm_0_vtxY"],data["Xibm_0_vtxZ"]]
dir_Xib0 = dir(vtx_Xib0, vtx_Xibm)
#p_Xi0b is not known, but its direction is. The simulated data for p_Xi0b is used to get the flight direction as unit vector
p_Xib0 = [data["Xibm_0_PX"], data["Xibm_0_PY"], data["Xibm_0_PZ"]]

#previously calculated values
Q = 0.5*(5.9523**2-0.13957018**2-5.7945**2)#GeV²
M = 5.7945 #GeV Mass of Xib'-
V = scalar_product(ppi, dir_Xib0)
#V = scalar_product(ppi, unitvec(p_ M_S = 1617 GeV for simulationpe="step",bins=75)

plt.figure(figsize=(10,5), dpi=80)
n, bins, _ = plt.hist(P_abs(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution =1)
                      ,histtype="step",bins=100, color = 'b', label = '+ solution, vtx method '
                      + str(infvals(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution = 1)) 
                      + ' infinite values')



#now with pxi0b

V = scalar_product(ppi, unitvec(p_Xib0))

n, bins, _ = plt.hist(P_abs(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution =1)
                      ,histtype="step",bins=100, color = 'cornflowerblue', label = '+ solution, with p_Xib, '
                      + str(infvals(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution = 1)) 
                      + ' infinite values')


plt.title('Sexaquarkmass $M_S$, 100 000 simulated decays, $\Xi_b(5945)^0$ Tag, $J/\psi$ final state', fontsize=14, family ='monospace')
plt.ylabel('Events / Bin ', fontsize=14, family ='monospace')
plt.xlabel('$M_S$ [GeV], $M_S$ = 1759 GeV for simulation', fontsize=14, family ='monospace')
#plt.savefig('fitted_data.png')
plt.legend()
plt.grid()
plt.show()
plt.grid()
plt.show()

exit(0)

#####################################

############################

data = read_rapidsim("Xistar")
p_pip= [data["pip_0_PX"],data["pip_0_PY"],data["pip_0_PZ"]]
E_pip = data["pip_0_E"]
p_Jspi = [data["Jpsi_0_PX"],data["Jpsi_0_PY"],data["Jpsi_0_PZ"]]
E_Jspi = data["Jpsi_0_E"]

p_Kaon =np.add(p_pip,p_Jspi)
E_Kaon = np.add(E_pip ,E_Jspi)

p_proton = [data["pm_0_PX"],data["pm_0_PY"],data["pm_0_PZ"]]
E_proton = data["pm_0_E"]
ppi =  [data["pim_0_PX"],data["pim_0_PY"],data["pim_0_PZ"]]
E_pi = data["pim_0_E"]
E = E_pi
vtx_Xib0= [data["Xib0_0_vtxX"], data["Xib0_0_vtxY"], data["Xib0_0_vtxZ"]]
vtx_Xibm = [data["Xistbm_0_vtxX"],data["Xistbm_0_vtxY"],data["Xistbm_0_vtxZ"]]
dir_Xib0 = dir(vtx_Xib0, vtx_Xibm)
#p_Xi0b is not known, but its direction is. The simulated data for p_Xi0b is used to get the flight direction as unit vector
p_Xib0 = [data["Xib0_0_PX"], data["Xib0_0_PY"], data["Xib0_0_PZ"]]

#previously calculated values
Q = 0.950185  #GeV²
M = 5.7919#GeV Mass of Xib'-
V = scalar_product(ppi, dir_Xib0)
#V = scalar_product(ppi, unitvec(p_ M_S = 1617 GeV for simulationpe="step",bins=75)
# exit(0)
plt.figure(figsize=(10,5), dpi=80)
n, bins, _ = plt.hist(P_abs(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution =1)
                      ,histtype="step",bins=100, color = 'b', label = '+ solution, vtx method '
                      + str(infvals(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution = 1)) 
                      + ' infinite values')



#now with pxi0b

V = scalar_product(ppi, unitvec(p_Xib0))

n, bins, _ = plt.hist(P_abs(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution =1)
                      ,histtype="step",bins=100, color = 'cornflowerblue', label = '+ solution, with p_Xib0, '
                      + str(infvals(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution = 1)) 
                      + ' infinite values')

###########################################



plt.title('Sexaquarkmass $M_S$, 100 000 simulated decays, $\Xi_b^{*}$ Tag, $J/\psi$ final state', fontsize=14, family ='monospace')
plt.ylabel('Events / Bin ', fontsize=14, family ='monospace')
plt.xlabel('$M_S$ [GeV], $M_S$ = 1617 GeV for simulation', fontsize=14, family ='monospace')
#plt.savefig('fitted_data.png')
plt.legend()
plt.grid()
plt.show()


###################################

import matplotlib.pyplot as plt
from read_data import read_rapidsim
from functions import P_abs, unitvec, scalar_product, infvals, dir
import numpy as np


data = read_rapidsim("Xi5945")
p_Jspi = [data["Jpsi_0_PX"],data["Jpsi_0_PY"],data["Jpsi_0_PZ"]]
E_Jspi = data["Jpsi_0_E"]

p_Kaon =p_Jspi
E_Kaon =E_Jspi

p_proton = [data["pm_0_PX"],data["pm_0_PY"],data["pm_0_PZ"]]
p_Kaon =p_Jspi
E_Kaon =E_Jspi

p_proton = [data["pm_0_PX"],data["pm_0_PY"],data["pm_0_PZ"]]
E_proton = data["pm_0_E"]
ppi =  [data["pip_0_PX"],data["pip_0_PY"],data["pip_0_PZ"]]
E_pi = data["pip_0_E"]
E = E_pi
vtx_Xibm= [data["Xib05945_0_vtxX"], data["Xib05945_0_vtxY"], data["Xib05945_0_vtxZ"]]
vtx_Xib0 = [data["Xibm_0_vtxX"],data["Xibm_0_vtxY"],data["Xibm_0_vtxZ"]]
dir_Xib0 = dir(vtx_Xib0, vtx_Xibm)
#p_Xi0b is not known, but its direction is. The simulated data for p_Xi0b is used to get the flight direction as unit vector
p_Xib0 = [data["Xibm_0_PX"], data["Xibm_0_PY"], data["Xibm_0_PZ"]]

#previously calculated values
Q = 0.5*(5.9523**2-0.13957018**2-5.7945**2)#GeV²
M = 5.7945 #GeV Mass of Xib'-
V = scalar_product(ppi, dir_Xib0)
#V = scalar_product(ppi, unitvec(p_ M_S = 1617 GeV for simulationpe="step",bins=75)
# exit(0)
plt.figure(figsize=(10,5), dpi=80)
n, bins, _ = plt.hist(P_abs(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution =1)
                      ,histtype="step",bins=100, color = 'b', label = '+ solution, vtx method '
                      + str(infvals(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution = 1)) 
                      + ' infinite values')



#now with pxi0b

V = scalar_product(ppi, unitvec(p_Xib0))

n, bins, _ = plt.hist(P_abs(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution =1)
                      ,histtype="step",bins=100, color = 'cornflowerblue', label = '+ solution, with p_Xib, '
                      + str(infvals(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution = 1)) 
                      + ' infinite values')


plt.title('Sexaquarkmass $M_S$, 100 000 simulated decays, $\Xi_b(5945)^0$ Tag, $J/\psi$ final state', fontsize=14, family ='monospace')
plt.ylabel('Events / Bin ', fontsize=14, family ='monospace')
plt.xlabel('$M_S$ [GeV], $M_S$ = 1759 GeV for simulation', fontsize=14, family ='monospace')
#plt.savefig('fitted_data.png')
plt.legend()
plt.grid()
plt.show()