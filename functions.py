import matplotlib.pyplot as plt
from read_data import read_rapidsim
import numpy as np


def unitvec(p):
    p = p/np.linalg.norm(p, axis=0)
    return p

def scalar_product(a,b):
    # expects arrays of shape (3, n)
    return np.sum(a * b, axis=0)

def dir(vtx_Xibm, vtx_Xib0):
    return np.subtract(np.array(vtx_Xibm),np.array(vtx_Xib0)) #funktioniert

def D(Q, E, V, M):
    return (16 * Q**2 * E**2 * V**2 * (Q**2 - M**2 * (E**2 - V**2)))  


def qboth(Q, E, V, M, solution ): 
    q1 = (Q**2 * (E**2+V**2) - M**2 * E**2 * (E**2-V**2) + np.sqrt(D(Q, E, V, M)))/( 2 * (E**2-V**2)**2)
    q2 = (Q**2 * (E**2+V**2) - M**2 * E**2 * (E**2-V**2) - np.sqrt(D(Q, E, V, M)))/( 2 * (E**2-V**2)**2)
    if solution == 1:
        return q1
    if solution == 2:
        return q2
    if solution == 3:
        return np.where(q1 >=0, q1,q2)


    raise ValueError(f"Solution must be either 1 , 2 or 3 not {solution}!")



def E_s(Q,E,V, M, E_proton, E_Kaon,solution ): 
    return (np.sqrt(M**2 + qboth(Q,E,V,M,solution ) ) - E_proton - E_Kaon)

def p_s(Q, E, V, M ,p_Xib0, p_proton, p_Kaon, solution ):
    v = unitvec(p_Xib0)
    return np.sqrt(qboth(Q, E, V, M,solution))*v - p_proton - p_Kaon


def P_abs(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution ):
    x = np.sqrt(E_s(Q,E,V, M, E_proton, E_Kaon, solution )**2 - np.sum(p_s(Q, E, V, M, p_Xib0, p_proton, p_Kaon, solution )**2, axis = 0 ))
    return x

def infvals(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution):
    x = P_abs(Q,E,V, M, E_proton, E_Kaon, p_Xib0, p_proton, p_Kaon,solution )
    y = np.where(np.isfinite(x), x , -3 * np.ones_like(x))
    return np.count_nonzero(y == -3)


