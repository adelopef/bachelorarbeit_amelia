import matplotlib.pyplot as plt
from read_data import read_rapidsim
from functions import P_abs, unitvec, scalar_product, infvals, dir, qboth
import numpy as np

m_Xib0 = 5.7919
m_Kaon = 0.493677
m_proton = 0.938272
m_Xibp = 5.935
data = read_rapidsim("Zerfall")
p_Kaon = [data["Kp_0_PX"],data["Kp_0_PY"],data["Kp_0_PZ"]]
E_Kaon = np.sqrt(m_Kaon**2 * np.ones_like(data["Kp_0_PX"])+ data["Kp_0_PX"]**2+data["Kp_0_PY"]**2+data["Kp_0_PZ"]**2)


p_proton = [data["pm_0_PX"],data["pm_0_PY"],data["pm_0_PZ"]]
E_proton = np.sqrt(m_proton**2 * np.ones_like(data["Kp_0_PX"])+ data["pm_0_PX"]**2+data["pm_0_PY"]**2 +data["pm_0_PZ"]**2)
P_ges = np.sqrt((E_Kaon+E_proton)**2- (data["pm_0_PX"]+data["Kp_0_PX"])**2-(data["pm_0_PY"]+data["Kp_0_PY"])**2-(data["pm_0_PZ"]+data["Kp_0_PZ"])**2)

n, bins, _ = plt.hist( P_ges
                      ,histtype="step",bins=100, color = 'r', label ='')

plt.title('Sexaquarkmass $M_S$, 100 000 simulated decays, $\Xi_b^{\'}$ Tag', fontsize=14, family ='monospace')
plt.ylabel('Events / Bin ', fontsize=14, family ='monospace')
plt.xlabel('$M_S$ [GeV]', fontsize=14, family ='monospace')
#plt.savefig('fitted_data.png')
plt.legend()
plt.grid()
plt.show()