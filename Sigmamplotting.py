import matplotlib.pyplot as plt
from read_data import read_rapidsim
from genfunctions import P_abs, unitvec, scalar_product, infvals, dir
import numpy as np
import array as arr

#Masses for P0 -> {P1 -> S, P1A, P1B} 

M_P0 =5.8155
M_P1 = 5.61951	
M_P2 = 0.13957018	

#data from simulation 

data = read_rapidsim("SigmaSK")
p_P1A= [data["Kp_0_PX"],data["Kp_0_PY"],data["Kp_0_PZ"]]
E_P1A = data["Kp_0_E"]
p_P1B = [data["pm_0_PX"],data["pm_0_PY"],data["pm_0_PZ"]]
E_P1B = data["pm_0_E"]
p_P2 =  [data["pim_0_PX"],data["pim_0_PY"],data["pim_0_PZ"]]
E_P1 = data["pim_0_E"]
E_P1 = E_P1
vtx_P1= [data["Lambdab0_0_origX"], data["Lambdab0_0_origY"], data["Lambdab0_0_origZ"]]
vtx_P0 = [data["Lambdab0_0_vtxX"],data["Lambdab0_0_vtxY"],data["Lambdab0_0_vtxZ"]]
dir_P1 = dir(vtx_P1, vtx_P0)
#p_Xi0b is not known, but its direction is. The simulated data for p_Xi0b is used to get the flight direction as unit vector
p_P1 = [data["Lambdab0_0_PX"], data["Lambdab0_0_PY"], data["Lambdab0_0_PZ"]]



V = scalar_product(p_P2, dir_P1)

#n, bins = np.histogram(p_P2)
#n, bins, _ = plt.hist(p_P2[0],histtype="step",bins=75)
# exit(0)
plt.figure(figsize=(10,5), dpi=80)
n, bins, _ = plt.hist(P_abs(M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution =1)
                      ,histtype="step",bins=100, color = 'b', label = '+ solution, vtx method '
                      + str(infvals( M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution = 1)) 
                      + ' infinite values')
#now with pxi0b

plt.title('Sexaquarkmass $M_S$ with Background, 100 000 simulated decays, $\Sigma_b^{-}$ Tag', fontsize=14, family ='monospace')
plt.ylabel('Events / Bin ', fontsize=14, family ='monospace')
plt.xlabel('$M_S$ [GeV]', fontsize=14, family ='monospace')
#plt.savefig('fitted_data.png')
plt.legend()
plt.grid()
plt.show()

#########################################

data = read_rapidsim("SigmaLambdaK")
p_P1A= [data["Kp_0_PX"],data["Kp_0_PY"],data["Kp_0_PZ"]]
E_P1A = data["Kp_0_E"]
p_P1B = [data["pm_0_PX"],data["pm_0_PY"],data["pm_0_PZ"]]
E_P1B = data["pm_0_E"]
p_P2 =  [data["pim_0_PX"],data["pim_0_PY"],data["pim_0_PZ"]]
E_P1 = data["pim_0_E"]
E_P1 = E_P1
vtx_P1= [data["Lambdab0_0_origX"], data["Lambdab0_0_origY"], data["Lambdab0_0_origZ"]]
vtx_P0 = [data["Lambdab0_0_vtxX"],data["Lambdab0_0_vtxY"],data["Lambdab0_0_vtxZ"]]
dir_P1 = dir(vtx_P1, vtx_P0)
#p_Xi0b is not known, but its direction is. The simulated data for p_Xi0b is used to get the flight direction as unit vector
p_P1 = [data["Lambdab0_0_PX"], data["Lambdab0_0_PY"], data["Lambdab0_0_PZ"]]



V = scalar_product(p_P2, dir_P1)


n, bins, _ = plt.hist(P_abs(M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution =1)
                      ,histtype="step",bins=100, color = 'r', label = '$\Lambda \Lambda$ mode, vtx method '
                      + str(infvals( M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution = 1)) 
                      + ' infinite values')

##########################################

data = read_rapidsim("SigmaXiK")
p_P1A= [data["Kp_0_PX"],data["Kp_0_PY"],data["Kp_0_PZ"]]
E_P1A = data["Kp_0_E"]
p_P1B = [data["pm_0_PX"],data["pm_0_PY"],data["pm_0_PZ"]]
E_P1B = data["pm_0_E"]
p_P2 =  [data["pim_0_PX"],data["pim_0_PY"],data["pim_0_PZ"]]
E_P1 = data["pim_0_E"]
E_P1 = E_P1
vtx_P1= [data["Lambdab0_0_origX"], data["Lambdab0_0_origY"], data["Lambdab0_0_origZ"]]
vtx_P0 = [data["Lambdab0_0_vtxX"],data["Lambdab0_0_vtxY"],data["Lambdab0_0_vtxZ"]]
dir_P1 = dir(vtx_P1, vtx_P0)
#p_Xi0b is not known, but its direction is. The simulated data for p_Xi0b is used to get the flight direction as unit vector
p_P1 = [data["Lambdab0_0_PX"], data["Lambdab0_0_PY"], data["Lambdab0_0_PZ"]]



V = scalar_product(p_P2, dir_P1)


n, bins, _ = plt.hist(P_abs(M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution =1)
                      ,histtype="step",bins=100, color = 'g', label = '$\Xi_0$ $n$ mode, vtx method '
                      + str(infvals( M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution = 1)) 
                      + ' infinite values')


#############################################


plt.title('Sexaquarkmass $M_S$ with Background, 100 000 simulated decays, $\Sigma_b^{-}$ Tag', fontsize=14, family ='monospace')
plt.ylabel('Events / Bin ', fontsize=14, family ='monospace')
plt.xlabel('$M_S$ [GeV]', fontsize=14, family ='monospace')
#plt.savefig('fitted_data.png')
plt.legend()
plt.grid()
plt.show()



#plt.grid()

#centers = (bins[1:] + bins[:-1])/2
#plt.errorbar(centers, n, fmt=".")
#plt.show()



####################################
import matplotlib.pyplot as plt
from read_data import read_rapidsim
from genfunctions import P_abs, unitvec, scalar_product, infvals, dir, Q
import numpy as np
import array as arr

#Masses for P0 -> {P1 -> S, P1A, P1B} 

M_P0 =5.8155
M_P1 = 5.61951	
M_P2 = 0.13957018	


#data from simulation 

data = read_rapidsim("SigmaSDs")
p_P1A= [data["Dsp_0_PX"],data["Dsp_0_PY"],data["Dsp_0_PZ"]]
E_P1A = data["Dsp_0_E"]
p_P1B = [data["pm_0_PX"],data["pm_0_PY"],data["pm_0_PZ"]]
E_P1B = data["pm_0_E"]
p_P2 =  [data["pim_0_PX"],data["pim_0_PY"],data["pim_0_PZ"]]
E_P1 = data["pim_0_E"]
E_P1 = E_P1
vtx_P1= [data["Lambdab0_0_origX"], data["Lambdab0_0_origY"], data["Lambdab0_0_origZ"]]
vtx_P0 = [data["Lambdab0_0_vtxX"],data["Lambdab0_0_vtxY"],data["Lambdab0_0_vtxZ"]]
dir_P1 = dir(vtx_P1, vtx_P0)
#p_Xi0b is not known, but its direction is. The simulated data for p_Xi0b is used to get the flight direction as unit vector
p_P1 = [data["Lambdab0_0_PX"], data["Lambdab0_0_PY"], data["Lambdab0_0_PZ"]]



V = scalar_product(p_P2, dir_P1)

#n, bins = np.histogram(p_P2)
#n, bins, _ = plt.hist(p_P2[0],histtype="step",bins=75)
# exit(0)
plt.figure(figsize=(10,5), dpi=80)
n, bins, _ = plt.hist(P_abs(M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution =1)
                      ,histtype="step",bins=100, color = 'b', label = '$S$ mode, vtx method '
                      + str(infvals( M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution = 1)) 
                      + ' infinite values')
data = read_rapidsim("SigmaSK")
p_P1A= [data["Kp_0_PX"],data["Kp_0_PY"],data["Kp_0_PZ"]]
E_P1A = data["Kp_0_E"]
p_P1B = [data["pm_0_PX"],data["pm_0_PY"],data["pm_0_PZ"]]
E_P1B = data["pm_0_E"]
p_P2 =  [data["pim_0_PX"],data["pim_0_PY"],data["pim_0_PZ"]]
E_P1 = data["pim_0_E"]
E_P1 = E_P1
vtx_P1= [data["Lambdab0_0_origX"], data["Lambdab0_0_origY"], data["Lambdab0_0_origZ"]]
vtx_P0 = [data["Lambdab0_0_vtxX"],data["Lambdab0_0_vtxY"],data["Lambdab0_0_vtxZ"]]
dir_P1 = dir(vtx_P1, vtx_P0)
#p_Xi0b is not known, but its direction is. The simulated data for p_Xi0b is used to get the flight direction as unit vector
p_P1 = [data["Lambdab0_0_PX"], data["Lambdab0_0_PY"], data["Lambdab0_0_PZ"]]



V = scalar_product(p_P2, dir_P1)

#n, bins = np.histogram(p_P2)
#n, bins, _ = plt.hist(p_P2[0],histtype="step",bins=75)
# exit(0)
plt.figure(figsize=(10,5), dpi=80)
n, bins, _ = plt.hist(P_abs(M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution =1)
                      ,histtype="step",bins=100, color = 'b', label = '+ solution, vtx method '
                      + str(infvals( M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution = 1)) 
                      + ' infinite values')
n, bins, _ = plt.hist(P_abs(M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution =2)
                      ,histtype="step",bins=100, color = 'r', label = '- solution, vtx method '
                      + str(infvals( M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution = 2)) 
                      + ' infinite values')

V = scalar_product(p_P2, unitvec(p_P1))

n, bins, _ = plt.hist(P_abs(M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution =1)
                      ,histtype="step",bins=100, color = 'cornflowerblue', label = '+ solution, p_p1 method '
                      + str(infvals( M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution = 1)) 
                      + ' infinite values')
n, bins, _ = plt.hist(P_abs(M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution =2)
                      ,histtype="step",bins=100, color = 'lightcoral', label = '- solution, p_p1 method '
                      + str(infvals( M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution = 2)) 
                      + ' infinite values')

#now with pxi0b

plt.title('Sexaquarkmass $M_S$ with Background, 100 000 simulated decays, $\Sigma_b^{-}$ Tag', fontsize=14, family ='monospace')
plt.ylabel('Events / Bin ', fontsize=14, family ='monospace')
plt.xlabel('$M_S$ [GeV]', fontsize=14, family ='monospace')
#plt.savefig('fitted_data.png')
plt.legend()
plt.grid()
plt.show()
exit(0)

#########################################
#now with pxi0b

#########################################

data = read_rapidsim("SigmaLambdaDs")
p_P1A= [data["Dsp_0_PX"],data["Dsp_0_PY"],data["Dsp_0_PZ"]]
E_P1A = data["Dsp_0_E"]
p_P1B = [data["pm_0_PX"],data["pm_0_PY"],data["pm_0_PZ"]]
E_P1B = data["pm_0_E"]
p_P2 =  [data["pim_0_PX"],data["pim_0_PY"],data["pim_0_PZ"]]
E_P1 = data["pim_0_E"]
E_P1 = E_P1
vtx_P1= [data["Lambdab0_0_origX"], data["Lambdab0_0_origY"], data["Lambdab0_0_origZ"]]
vtx_P0 = [data["Lambdab0_0_vtxX"],data["Lambdab0_0_vtxY"],data["Lambdab0_0_vtxZ"]]
dir_P1 = dir(vtx_P1, vtx_P0)
#p_Xi0b is not known, but its direction is. The simulated data for p_Xi0b is used to get the flight direction as unit vector
p_P1 = [data["Lambdab0_0_PX"], data["Lambdab0_0_PY"], data["Lambdab0_0_PZ"]]



V = scalar_product(p_P2, dir_P1)


n, bins, _ = plt.hist(P_abs(M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution =1)
                      ,histtype="step",bins=100, color = 'r', label = '$\Lambda \Lambda$ mode, vtx method '
                      + str(infvals( M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution = 1)) 
                      + ' infinite values')

##########################################

data = read_rapidsim("SigmaXiDs")
p_P1A= [data["Dsp_0_PX"],data["Dsp_0_PY"],data["Dsp_0_PZ"]]
E_P1A = data["Dsp_0_E"]
p_P1B = [data["pm_0_PX"],data["pm_0_PY"],data["pm_0_PZ"]]
E_P1B = data["pm_0_E"]
p_P2 =  [data["pim_0_PX"],data["pim_0_PY"],data["pim_0_PZ"]]
E_P1 = data["pim_0_E"]
E_P1 = E_P1
vtx_P1= [data["Sigmabm_0_vtxX"], data["Sigmabm_0_vtxY"], data["Sigmabm_0_vtxZ"]]
vtx_P0 = [data["Lambdab0_0_vtxX"],data["Lambdab0_0_vtxY"],data["Lambdab0_0_vtxZ"]]
dir_P1 = dir(vtx_P1, vtx_P0)
#p_Xi0b is not known, but its direction is. The simulated data for p_Xi0b is used to get the flight direction as unit vector
p_P1 = [data["Lambdab0_0_PX"], data["Lambdab0_0_PY"], data["Lambdab0_0_PZ"]]



V = scalar_product(p_P2, dir_P1)


n, bins, _ = plt.hist(P_abs(M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution =1)
                      ,histtype="step",bins=100, color = 'g', label = '$\Xi_0$ $n$ mode, vtx method '
                      + str(infvals( M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution = 1)) 
                      + ' infinite values')


#############################################


plt.title('Sexaquarkmass $M_S$ with Background, 100 000 simulated decays, $\Sigma_b^{-}$ Tag, $D_s$ mode', fontsize=14, family ='monospace')
plt.ylabel('Events / Bin ', fontsize=14, family ='monospace')
plt.xlabel('$M_S$ [GeV]', fontsize=14, family ='monospace')
#plt.savefig('fitted_data.png')
plt.legend()
plt.grid()
plt.show()

##########################################################

#plt.grid()

#centers = (bins[1:] + bins[:-1])/2
#plt.errorbar(centers, n, fmt=".")
#plt.show()


data = read_rapidsim("SigmaSK")
p_P1A= [data["Kp_0_PX"],data["Kp_0_PY"],data["Kp_0_PZ"]]
E_P1A = data["Kp_0_E"]
p_P1B = [data["pm_0_PX"],data["pm_0_PY"],data["pm_0_PZ"]]
E_P1B = data["pm_0_E"]
p_P2 =  [data["pim_0_PX"],data["pim_0_PY"],data["pim_0_PZ"]]
E_P1 = data["pim_0_E"]
E_P1 = E_P1
vtx_P1= [data["Lambdab0_0_origX"], data["Lambdab0_0_origY"], data["Lambdab0_0_origZ"]]
vtx_P0 = [data["Lambdab0_0_vtxX"],data["Lambdab0_0_vtxY"],data["Lambdab0_0_vtxZ"]]
dir_P1 = dir(vtx_P1, vtx_P0)
#p_Xi0b is not known, but its direction is. The simulated data for p_Xi0b is used to get the flight direction as unit vector
p_P1 = [data["Lambdab0_0_PX"], data["Lambdab0_0_PY"], data["Lambdab0_0_PZ"]]


V = scalar_product(p_P2, dir_P1)

#n, bins = np.histogram(p_P2)
#n, bins, _ = plt.hist(p_P2[0],histtype="step",bins=75)
# exit(0)
plt.figure(figsize=(10,5), dpi=80)
n, bins, _ = plt.hist(P_abs(M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution =1)
                      ,histtype="step",bins=100, color = 'b', label = '+ solution, vtx method '
                      + str(infvals( M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution = 1)) 
                      + ' infinite values')
n, bins, _ = plt.hist(P_abs(M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution =2)
                      ,histtype="step",bins=100, color = 'r', label = '- solution, vtx method '
                      + str(infvals( M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution = 2)) 
                      + ' infinite values')

V = scalar_product(p_P2, unitvec(p_P1))

n, bins, _ = plt.hist(P_abs(M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution =1)
                      ,histtype="step",bins=100, color = 'cornflowerblue', label = '+ solution, p_p1 method '
                      + str(infvals( M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution = 1)) 
                      + ' infinite values')
n, bins, _ = plt.hist(P_abs(M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution =2)
                      ,histtype="step",bins=100, color = 'lightcoral', label = '- solution, p_p1 method '
                      + str(infvals( M_P1, M_P2,E_P1,V, M_P0, E_P1B, E_P1A, p_P1, p_P1B, p_P1A,solution = 2)) 
                      + ' infinite values')

#now with pxi0b

plt.title('Sexaquarkmass $M_S$ with Background, 100 000 simulated decays, $\Sigma_b^{-}$ Tag', fontsize=14, family ='monospace')
plt.ylabel('Events / Bin ', fontsize=14, family ='monospace')
plt.xlabel('$M_S$ [GeV]', fontsize=14, family ='monospace')
#plt.savefig('fitted_data.png')
plt.legend()
plt.grid()
plt.show()
exit(0)

#########################################


